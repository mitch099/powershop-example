//
//  Styles.m
//  MagicViewExample
//
//  Created by Mitch on 3/09/15.
//  Copyright (c) 2015 ibdn. All rights reserved.
//

#import "Styles.h"
#import "FUIButton.h"
#import "FUITextField.h"
#import "FUISwitch.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"

@implementation Styles

#pragma mark - Style Handler
-(id)addStyle:(NSString *)Style Object:(id)Object {
    //Seperate by spaces
    NSArray *Styles = [Style componentsSeparatedByString:@" "];
    
    //Loop over the sttyles and add..
    for(NSString *StyleString in Styles) {
        
        //Build Selector
        SEL customStyle = NSSelectorFromString([NSString stringWithFormat:@"%@:", StyleString]);
        
        //Check we can actually call it
        if([self respondsToSelector:customStyle]) {
            //Build the object..
            Object = ((id (*)(id, SEL, id))[self methodForSelector:customStyle])(self, customStyle, Object);
        }
    }
    
    //Return it
    return Object;
}


-(id)btn_flat:(FUIButton *)View {
    View.frame = [self _padding:View Padding:5];
    View.buttonColor = [UIColor turquoiseColor];
    View.shadowColor = [UIColor greenSeaColor];
    View.shadowHeight = 3.0f;
    View.cornerRadius = 6.0f;
    View.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    [View setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [View setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
    return View;
}

-(id)input_flat:(FUITextField *)View {
    View.font = [UIFont flatFontOfSize:16];
    View.backgroundColor = [UIColor clearColor];
    View.edgeInsets = UIEdgeInsetsMake(4.0f, 15.0f, 4.0f, 15.0f);
    View.textFieldColor = [UIColor sunflowerColor];
    View.borderColor = [UIColor turquoiseColor];
    View.borderWidth = 2.0f;
    View.cornerRadius = 3.0f;
    return View;
}

-(id)h1_flat:(UILabel *)View {
    [View setTextColor:[UIColor wisteriaColor]];
    [View setFont:[UIFont fontWithName:@"HelveticaNeue" size:60]];
    return View;
}
-(id)h2_flat:(UILabel *)View {
    [View setTextColor:[UIColor wisteriaColor]];
    [View setFont:[UIFont fontWithName:@"HelveticaNeue" size:26]];
    return View;
}
-(id)h3_flat:(UILabel *)View {
    [View setTextColor:[UIColor wisteriaColor]];
    [View setFont:[UIFont fontWithName:@"HelveticaNeue" size:24]];
    return View;
}
-(id)h4_flat:(UILabel *)View {
    [View setTextColor:[UIColor wisteriaColor]];
    [View setFont:[UIFont fontWithName:@"HelveticaNeue" size:22]];
    return View;
}
-(id)p_flat:(UILabel *)View {
    [View setTextColor:[UIColor blackColor]];
    [View setFont:[UIFont fontWithName:@"HelveticaNeue" size:18]];
    return View;
}
-(id)text_center:(UILabel *)View {
    [View setTextAlignment:NSTextAlignmentCenter];
    return View;
}
-(id)text_right:(UILabel *)View {
    [View setTextAlignment:NSTextAlignmentRight];
    return View;
}

-(id)form_pad:(id)View {
    [View setFrame:[self _padding:View Padding:5]];
    return View;
}

-(id)separator:(UIView *)View {
    [View setBackgroundColor:[UIColor blackColor]];
    [View setAlpha:0.5];
    return View;
}

#pragma mark - Animations

-(id)animate_fadein:(UIView *)View {
    View.alpha = 0.0f;
    [UIView animateWithDuration:1.0f animations:^{
        View.alpha = 1.0f;;
    }];
    return View;
}

-(id)animate_topin:(UIView *)View {
    CGRect finish_frame = [View frame];
    CGRect start_frame = CGRectMake(finish_frame.origin.x, (0 - finish_frame.size.height), finish_frame.size.width, finish_frame.size.height);
    
    View.frame = start_frame;
    [UIView animateWithDuration:1.0f animations:^{
        View.frame = finish_frame;
    }];
    
    return View;
}

-(id)animate_bottomin:(UIView *)View {
    CGRect finish_frame = [View frame];
    CGRect start_frame = CGRectMake(finish_frame.origin.x, [UIScreen mainScreen].applicationFrame.size.height, finish_frame.size.width, finish_frame.size.height);
    
    View.frame = start_frame;
    [UIView animateWithDuration:1.0f animations:^{
        View.frame = finish_frame;
    }];
    
    return View;
}

-(id)animate_leftin:(UIView *)View {
    CGRect finish_frame = [View frame];
    CGRect start_frame = CGRectMake((0 - finish_frame.size.width), finish_frame.origin.y, finish_frame.size.width, finish_frame.size.height);
    
    View.frame = start_frame;
    [UIView animateWithDuration:1.0f animations:^{
        View.frame = finish_frame;
    }];
    
    return View;
}

-(id)animate_rightin:(UIView *)View {
    CGRect finish_frame = [View frame];
    CGRect start_frame = CGRectMake([UIScreen mainScreen].applicationFrame.size.width, finish_frame.origin.y, finish_frame.size.width, finish_frame.size.height);
    
    View.frame = start_frame;
    [UIView animateWithDuration:1.0f animations:^{
        View.frame = finish_frame;
    }];
    
    return View;
}


#pragma mark - Style Helper functions
//Pad Left and Right
-(CGRect)_padding:(id)View Padding:(int)Padding {
    //Get the frame
    CGRect frame = [View frame];
    //Left
    frame.origin.x += Padding;
    frame.size.width -= Padding;
    //Right
    frame.size.width -= Padding;

    //Return Value
    return frame;
}
//Pad Left
-(CGRect)_paddingLeft:(id)View Padding:(int)Padding {
    //Get the frame
    CGRect frame = [View frame];
    //Left
    frame.origin.x += Padding;
    frame.size.width -= Padding;
    
    //Return Value
    return frame;
}
//Pad Right
-(CGRect)_paddingRight:(id)View Padding:(int)Padding {
    //Get the frame
    CGRect frame = [View frame];
    //Left
    frame.origin.x += Padding;
    frame.size.width -= Padding;
    
    //Return Value
    return frame;
}



@end
