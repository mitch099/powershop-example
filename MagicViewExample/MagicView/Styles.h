//
//  Styles.h
//  MagicViewExample
//
//  Created by Mitch on 3/09/15.
//  Copyright (c) 2015 ibdn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Styles : NSObject

-(id)addStyle:(NSString *)Style Object:(id)Object;

@end
