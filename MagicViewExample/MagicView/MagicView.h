//
//  MagicView.h
//
//
//  Created by Mitch Harris on 4/09/15.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MagicView : NSObject

@property (nonatomic, retain) NSMutableDictionary *viewsIndex;

@property (nonatomic) float PADDING;
@property (nonatomic) float MAX_COL;

@property (nonatomic) float X;
@property (nonatomic) float Y;
@property (nonatomic) float Width;
@property (nonatomic) float Height;

-(id)addViewManual:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID X:(float)X Width:(float)Width Y:(float)Y Height:(float)Height;
-(id)addView:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID Col:(int)Col Colspan:(float)Colspan Height:(float)Height;
-(id)addView:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID Col:(int)Col Colspan:(float)Colspan Row:(float)Row Height:(float)Height;
-(id)addView:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID Col:(int)Col Colspan:(float)Colspan Y:(float)Y Height:(float)Height;
-(NSString *)getValue:(NSString *)ID;
-(float)getViewHeight:(id)View;
-(float)getViewRealestate:(id)View;
-(id)getView:(NSString *)ID;
@end
