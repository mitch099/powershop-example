//
//  MagicView.m
//
//
//  Created by Mitch Harris on 4/09/15.
//
//

#import "MagicView.h"
#import "Styles.h"


#define PADDING_DEFAULT ((int) 5)
#define COL_DEFAULT ((int) 12)

@implementation MagicView

-(id)init {
    _viewsIndex = [[NSMutableDictionary alloc] init];
    _PADDING = PADDING_DEFAULT;
    _MAX_COL = COL_DEFAULT;
    return self;
}
-(id)init:(int)Padding Maxcol:(int)Maxcol {
    _viewsIndex = [[NSMutableDictionary alloc] init];
    _PADDING = Padding;
    _MAX_COL = Maxcol;
    return self;
}
-(id)initWithPadding:(int)Padding {
    _viewsIndex = [[NSMutableDictionary alloc] init];
    _PADDING = Padding;
    _MAX_COL = COL_DEFAULT;
    return self;
}
-(id)initWithMaxcol:(int)Maxcol {
    _viewsIndex = [[NSMutableDictionary alloc] init];
    _PADDING = PADDING_DEFAULT;
    _MAX_COL = Maxcol;
    return self;
}

#pragma mark - Public Functions

-(id)addViewManual:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID X:(float)X Width:(float)Width Y:(float)Y Height:(float)Height {
    _Width = Width;
    _Height = Height;
    _X = X;
    _Y = Y;
    return [self createView:Type Parent:Parent Style:Style ID:ID];
}
-(id)addView:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID Col:(int)Col Colspan:(float)Colspan Height:(float)Height {
    _Width = [self buildWidthCol:Parent ColIndex:Colspan];
    _Height = Height;
    _X = [self buildCol:Parent ColIndex:Col];
    _Y = [self buildRow:Parent Col:Col Colspan:Colspan];
    return [self createView:Type Parent:Parent Style:Style ID:ID];
}
-(id)addView:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID Col:(int)Col Colspan:(float)Colspan Row:(float)Row Height:(float)Height {
    _Width = [self buildWidthCol:Parent ColIndex:Colspan];
    _Height = Height;
    _X = [self buildCol:Parent ColIndex:Col];
    _Y = [self buildRowInline:Parent RowIndex:Row Col:Col Colspan:Colspan];
    return [self createView:Type Parent:Parent Style:Style ID:ID];
}
-(id)addView:(id)Type Parent:(UIView *)Parent Style:(NSString * )Style ID:(NSString *)ID Col:(int)Col Colspan:(float)Colspan Y:(float)Y Height:(float)Height {
    _Width = [self buildWidthCol:Parent ColIndex:Colspan];
    _Height = Height;
    _X = [self buildCol:Parent ColIndex:Col];
    _Y = Y;
    return [self createView:Type Parent:Parent Style:Style ID:ID];
}
-(float)getViewHeight:(id)View {
    return [self buildAbsoluteRow:View];
}
-(float)getViewRealestate:(id)View {
    return ([View frame].size.height - [self buildAbsoluteRow:View]);
}
-(NSString *)getValue:(NSString *)ID {
    NSString *value = @"";
    //Check if the object exists
    if([_viewsIndex objectForKey:ID]) {
        //Get the view
        id view = [_viewsIndex objectForKey:ID];
        //Check type
        if([view isKindOfClass:[UITextField class]]) value = [view text]; //TextField
        else if([view isKindOfClass:[UILabel class]]) value = [view text]; //Label
        else if([view isKindOfClass:[UIPickerView class]]) value = [NSString stringWithFormat:@"%ld", (long)[view selectedRowInComponent:0]]; //PickerView - Only first column at this point
    }
    //Return value
    return value;
}
-(id)getView:(NSString *)ID {
    //Check if the object exists
    if([_viewsIndex objectForKey:ID]) {
        //Get the view
        return[_viewsIndex objectForKey:ID];
    }
    //Return false if no found
    return false;
}



#pragma mark - Private Functions

//  Builds the new view
//
//  Type id - Type of new view.
//  Parent UIView - View we are adding view to.
//  Style NSString - Styles we want to apply to the new view.
//  ID - NSString - ID for the new view.
//
-(id)createView:(id)Type Parent:(UIView *)Parent Style:(NSString *)Style ID:(NSString *)ID {
    //Create the view with frame
    id newView = [[([Type isKindOfClass:[NSString class]] ? [NSClassFromString(Type) class] : Type) alloc] initWithFrame: CGRectMake(_X, _Y, _Width, _Height)];
    //Add styles
    [[Styles alloc] addStyle:Style Object:newView];
    //Add the view to the parent
    [Parent addSubview:newView];
    //Add the view to the index array of items in view
    [self addViewToIndex:newView ID:ID];
    //Reset the frame
    [self reset];
    //Return the view
    return newView;
}

//  Adds the view to index with unique id
//
//  View id - View to index.
//  ID - NSString - ID for the new view.
//
-(void)addViewToIndex:(id)View ID:(NSString *)ID {
    if(![ID isEqualToString:@""]) [_viewsIndex setObject:View forKey:ID];
}

//  Resets the values for frame
//
-(void)reset {
    _X = 0;
    _Y = 0;
    _Width = 0;
    _Height = 0;
}

#pragma mark - Frame Calculation Functions

//  Returns the X Value
//
//  Parent UIView - Expect view that we are adding the new view too.
//  ColIndex - int - Col that we want to be placed on.
//
-(float)buildCol:(UIView *)Parent ColIndex:(int)ColIndex {
    //Set column to zero
    float Col = 0;
    //Set the Allowed number of columns to zero
    int ColCount = _MAX_COL;
    
    //If column is more than available then just make it the max allowed....
    if(ColIndex > ColCount) ColIndex = ColCount;
    //Check if Column sent isnt zero based...
    if(ColIndex > 0) ColIndex -= 1;
    
    //Get our parents width
    float viewWidth = Parent.bounds.size.width;
    
    //Calculate the coulmn
    Col = ((viewWidth / ColCount) * ColIndex);
    
    //Return Value
    return Col;
}

//  Returns the next possible Y Value
//
//  Parent UIView - Expect view that we are adding the new view too.
//  Col - int - New views col
//  Colspan - int - New views colspan
//
-(float)buildRow:(UIView *)Parent Col:(int)Col Colspan:(int)ColSpan {
    //Default row as zero
    int Row = 0;
    
    //Build current items in the view
    NSMutableArray *objectsInView = [[Parent subviews] mutableCopy];
    
    //Remove guides from the array
    if([objectsInView count] != 0) {
        if([NSStringFromClass([[objectsInView objectAtIndex:0] class]) isEqualToString:@"_UILayoutGuide"]) {
            [objectsInView removeObjectsInRange:NSMakeRange(0, 2)];
        }
    }
    
    //If there is a status bar
    if(![UIApplication sharedApplication].isStatusBarHidden) {
        //Account for status bar
        Row += [UIApplication sharedApplication].statusBarFrame.size.height;
    }
    
    //Reverse the items in view so we work from the bottom up to save time!
    objectsInView = [[[objectsInView reverseObjectEnumerator] allObjects] mutableCopy];
    
    //Calculate the start and end points
    int newStart = Col;
    int newEnd = (Col + ColSpan) - 1;
    
    //Loop over objects and calculate the row
    for(id view in objectsInView) {
        //Get the current views Col & Colspan
        int viewCol = [self findCol:Parent x:[view frame].origin.x];
        int viewColSpan = ([self findCol:Parent x:[view frame].size.width]);
        //Calculate the start and end points
        int curStart = viewCol;
        int curEnd = (viewCol + viewColSpan) - 1;
        
        //If New views Col is between the start and end then we go under it
        if(newStart >= curStart && newStart <= curEnd) {
            //Add the row value
            Row = ([view frame].origin.y + [view frame].size.height) + _PADDING;
            //End for loop as we need to go no further
            break;
        }
        //If the Current views start is between the new views start and end we go under it
        else if(curStart >= newStart && curStart <= newEnd) {
            //Add the row value
            Row = ([view frame].origin.y + [view frame].size.height) + _PADDING;
            //End for loop as we need to go no further
            break;
        }
    }
    
    //Return Value
    return Row;
}

//  Returns the Y inline with Value
//
//  Parent UIView - Expect view that we are adding the new view too.
//  Col - int - New views col
//  Colspan - int - New views colspan
//
-(float)buildRowInline:(UIView *)Parent RowIndex:(int)RowIndex Col:(int)Col Colspan:(int)ColSpan {
    //Default row as zero
    int Row = 0;
    
    //Make row index zero based
    if(RowIndex > 0) RowIndex -= 1;
    
    //Build current items in the view
    NSMutableArray *objectsInView = [[Parent subviews] mutableCopy];
    
    //Remove us and the guides from the array
    if([objectsInView count] != 0) {
        if([NSStringFromClass([[objectsInView objectAtIndex:0] class]) isEqualToString:@"_UILayoutGuide"]) {
            [objectsInView removeObjectsInRange:NSMakeRange(0, 2)];
        }
    }
    
    //Check if index is in range...
    if([objectsInView count] >= (RowIndex + 1)) {
        Row = [[objectsInView objectAtIndex:RowIndex] frame].origin.y;
    }
    else {
        //Lets just return the next available postion in view...
        return [self buildRowInline:Parent RowIndex:RowIndex Col:Col Colspan:ColSpan];
    }
    
    //Return Value
    return Row;
}

//  Returns the actual bottom Y Value
//
//  Parent UIView - Expect view that we are adding the new view too.
//
-(float)buildAbsoluteRow:(UIView *)Parent {
    //Default row as zero
    int Row = 0;
    
    //Build current items in the view
    NSMutableArray *objectsInView = [[Parent subviews] mutableCopy];
    
    //Remove guides from the array
    if([objectsInView count] != 0) {
        if([NSStringFromClass([[objectsInView objectAtIndex:0] class]) isEqualToString:@"_UILayoutGuide"]) {
            [objectsInView removeObjectsInRange:NSMakeRange(0, 2)];
        }
    }
    
    //If there is a status bar
    if(![UIApplication sharedApplication].isStatusBarHidden) {
        //Account for status bar
        Row += [UIApplication sharedApplication].statusBarFrame.size.height;
    }
    
    if([objectsInView count] != 0) {
        //Loop over items...
        for(id View in objectsInView) {
            //Get the current row height
            int curRow = ([View frame].origin.y + [View frame].size.height) + _PADDING;
            //If it is larger then we make it the new row
            if(curRow > Row) Row = curRow;
        }
    }
    else {
        //No views so its top plus padding
        Row = _PADDING;
    }
    
    //Return Value
    return Row;
}

//  Returns the Width Value
//
//  Parent UIView - Expect view that we are adding the new view too.
//  ColIndex - int - Number of cols view will span.
//
-(float)buildWidthCol:(UIView *)Parent ColIndex:(int)ColIndex {
    //Set column to zero
    float Col = 0;
    //Set the Allowed number of columns to zero
    int ColCount = _MAX_COL;
    
    //If column is more than available then just make it the max allowed....
    if(ColIndex > ColCount) ColIndex = ColCount;
    
    //Get our parents width
    float viewWidth = Parent.bounds.size.width;
    
    //Calculate the coulmn
    Col = ((viewWidth / ColCount) * ColIndex);
    
    //Return Value
    return Col;
}

//  Returns the col the the value sits on
//
//  View UIView - View in question.
//  x - float - X value for the view.
//
-(int)findCol:(UIView *)Parent x:(float)x {
    //Set the Allowed number of columns to zero
    int ColCount = _MAX_COL;
    
    //Default col will span the width so max
    int Col = ColCount;
    
    //Get our parents width
    float viewWidth = Parent.bounds.size.width;
    
    for (int colIndex = 0; colIndex < ColCount; colIndex++) {
        
        float start = ((viewWidth / ColCount) * colIndex);
        float end = ((viewWidth / ColCount) * (colIndex+1));
        
        if(x >= start && x < end) {
            Col = (colIndex + 1);
            break;
        }
        
    }
    
    return Col;
}


@end
