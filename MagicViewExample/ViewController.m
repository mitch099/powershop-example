//
//  ViewController.m
//  MagicViewExample
//
//  Created by Mitch Harris on 4/09/15.
//  Copyright (c) 2015 Inbox Design. All rights reserved.
//

#import "ViewController.h"
#import "MagicView.h"
#import "FUIButton.h"
#import "FUITextField.h"
#import "FUISwitch.h"

@interface ViewController () {
    MagicView *mv;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    mv = [[MagicView alloc] init];
    
    //Build splash screen
    [self buildSplash];
}

//Splash Layout
-(void)buildSplash {
    [[mv addView:[UILabel class] Parent:self.view Style:@"h1_flat text_center animate_fadein" ID:@"" Col:2 Colspan:10 Y:40 Height:70]
     setText:@"Example"];
    
    [[mv addView:[UILabel class] Parent:self.view Style:@"p_flat text_center animate_fadein" ID:@"" Col:2 Colspan:10 Height:70]
     setText:@"Wooohoooo!!"];
    
    UIImageView *imageView = [mv addView:[UIImageView class] Parent:self.view Style:@"animate_leftin" ID:@"imageView" Col:2 Colspan:10 Height:250];
    imageView.image = [UIImage imageNamed:@"thumps"];
    
    [[mv addView:[FUIButton class] Parent:self.view Style:@"btn_flat animate_fadein" ID:@"more" Col:2 Colspan:10 Y:(self.view.frame.size.height - 75) Height:70]
     setTitle:@"More Please!!" forState:UIControlStateNormal];
    [[mv getView:@"more"] addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
}

//Info
-(void)buildInfo {
    [[mv addView:[UILabel class] Parent:self.view Style:@"h1_flat text_center animate_rightin" ID:@"" Col:2 Colspan:10 Y:40 Height:70]
     setText:@"Why..?"];
    
    UILabel *blurb = [mv addView:[UILabel class] Parent:self.view Style:@"p_flat text_center animate_fadein" ID:@"" Col:2 Colspan:10 Height:300];
    [blurb setText:@"Building views programmatically is now a piece of cake! Taking what use to be stacks of code and turning it into a simple and easy to read 1 line power house of efficiency!! How could this get better? Want to change the colour of a button through the whole project! Its now as simple as updating the style sheet!"];
    [blurb setNumberOfLines:0];
}

//Clear view
-(void)clear {
    for(id view in [self.view subviews]) {
        [UIView animateWithDuration:1.0f animations:^{
            [view setAlpha:0.0f];
        }];
    }
}

//Next
-(void)next {
    [self clear];
    [self performSelector:@selector(buildInfo) withObject:nil afterDelay:0.9f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
